import { Component } from '@angular/core';
import { variable } from '@angular/compiler/src/output/output_ast';
import { timer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  audioObject = new Audio();

  animais = [
    {nome: 'CAVALO',
     0: '../assets/img/cavalo1.png',
     1: '../assets/img/cavalo2.png',
     2: '../assets/img/cavalo3.png',
     3: '../assets/img/cavalo4.png',
     audio: '../assets/img/Cavalo.mp3'},
    {nome: 'FOCA',
     0: '../assets/img/foca1.png',
     1: '../assets/img/foca2.png',
     2: '../assets/img/foca3.png',
     3: '../assets/img/foca4.png',
     audio: '../assets/img/Foca.mp3'},
     {nome: 'GALO',
     0: '../assets/img/galo1.png',
     1: '../assets/img/galo2.png',
     2: '../assets/img/galo3.png',
     3: '../assets/img/galo4.png',
     audio: '../assets/img/Galo.mp3'},
     {nome: 'GATO',
     0: '../assets/img/gato1.png',
     1: '../assets/img/gato2.png',
     2: '../assets/img/gato3.png',
     3: '../assets/img/gato4.png',
     audio: '../assets/img/Gato.mp3'},
     {nome: 'PATO',
     0: '../assets/img/pato1.png',
     1: '../assets/img/pato2.png',
     2: '../assets/img/pato3.png',
     3: '../assets/img/pato4.png',
     audio: '../assets/img/Pato.mp3'},
     {nome: 'PORCO',
     0: '../assets/img/porco1.png',
     1: '../assets/img/porco2.png',
     2: '../assets/img/porco3.png',
     3: '../assets/img/porco4.png',
     audio: '../assets/img/Porco.mp3'},
     {nome: 'RATO',
     0: '../assets/img/rato1.png',
     1: '../assets/img/rato2.png',
     2: '../assets/img/rato3.png',
     3: '../assets/img/rato4.png',
     audio: '../assets/img/Rato.mp3'},
     {nome: 'SAPO',
     0: '../assets/img/sapo1.png',
     1: '../assets/img/sapo2.png',
     2: '../assets/img/sapo3.png',
     3: '../assets/img/sapo4.png',
     audio: '../assets/img/Sapo.mp3'},
     {nome: 'URSO',
     0: '../assets/img/urso1.png',
     1: '../assets/img/urso2.png',
     2: '../assets/img/urso3.png',
     3: '../assets/img/urso4.png',
     audio: '../assets/img/Urso.mp3'},
     {nome: 'VACA',
     0: '../assets/img/vaca1.png',
     1: '../assets/img/vaca2.png',
     2: '../assets/img/vaca3.png',
     3: '../assets/img/vaca4.png',
     audio: '../assets/img/Vaca.mp3'},
  ];

  /* Pontuação geral do dois jogadores */
  pontuacaoJogador1 = 0;
  pontuacaoJogador2 = 0;
  rodadasJogador1 = 0;
  rodadasJogador2 = 0;
  nomeJogador1 = null;
  nomeJogador2 = null;
  textoJogo = '';
  jogadorAtual = 1;

  display = 'hide';
  pontuacaoRodada = 1000;

  /* Imagem letra selecionada */
  imagemSelecionada = null;
  /* Index imagem clicada */
  indexImagem = 0;

  /* Variaveis para controle da imagem */
  indexAnimal = null;
  animalNome = null;
  src = null;

  /* Array de resposta */
  arrayResposta = [];

  /* Variaveis usadas no metodo montaArrayPalavra() */
  contadorArray = 0;
  arrayPalavra = [];

  /* Variavel usada para definir qual letra está sendo preenchida */
  posicaoPalavra = 0;

  /* Se o audio já foi reproduzido não deve mais ser descontado pontos doa rodada */
  audioReproduzido = false;

  /* Indica fim da rodada */
  fimRodada = false;

  modalInitialStatus = 'in';
  modalAlertStatus = 'off';
  modalAlertTexto1 = '';
  modalAlertTexto2 = '';

  constructor() {
    this.resetRodada();
  }

  trocaFoto() {
    this.indexImagem++;
    if (this.indexImagem < 4) {
      this.src = this.animais[this.indexAnimal][this.indexImagem];
      this.pontuacaoRodada = this.pontuacaoRodada - 100;
    }
  }

  executaAudio() {
    if (!this.audioReproduzido) {
      this.audioReproduzido = true;
      this.pontuacaoRodada = this.pontuacaoRodada - 500;
    }
    this.audioObject.src = this.animais[this.indexAnimal]['audio'];
    this.audioObject.play();
  }

  clickLetra(letra) {
    if (this.arrayResposta[this.posicaoPalavra] !== '__') {
      alert('Letra já preenchida! Selecione outra...');
      return;
    }

    this.imagemSelecionada = '../assets/img/alfabeto/' + letra + '.png';
    /*this.audioObject.src = this.animais[this.indexAnimal]['audio'];
    this.audioObject.play();*/
    this.display = 'show';
    if (this.arrayPalavra[this.posicaoPalavra] === letra) {
      this.arrayResposta[this.posicaoPalavra] = letra;
      this.selecionarPosicaoVaga();
      this.atualizaTexto(', parabéns, você acertou...');

      for (this.contadorArray = 0; this.contadorArray < this.animalNome.length; this.contadorArray++) {
        if (this.arrayResposta[this.contadorArray] !== '__') {
          this.fimRodada = true;
        } else {
          this.fimRodada = false;
          return;
        }
      }
    }
    if (this.fimRodada) {
      this.atualizaPontuacao();
      if (! this.verificaFimJogo()) {
        return;
      }
    }
    this.atualizaTexto(', letra errada, tente novamente...');
    this.pontuacaoRodada = this.pontuacaoRodada - 20;
  }

  selecionarPosicaoVaga() {
    for (this.contadorArray = 0; this.contadorArray < this.animalNome.length; this.contadorArray++) {
      if (this.arrayResposta[this.contadorArray] === '__') {
        this.posicaoPalavra = this.contadorArray;
        return;
      }
    }
  }

  verificaFimJogo() {
    if (this.pontuacaoJogador1 >= 2000) {
      alert(this.nomeJogador1 + ' você ganhou, sua pontuação foi de ' + this.pontuacaoJogador1 + ' pontos');
      location.reload();
      return true;
    }

    if (this.pontuacaoJogador2 >= 2000) {
      alert(this.nomeJogador2 + ' você ganhou, sua pontuação foi de ' + this.pontuacaoJogador2 + ' pontos');
      location.reload();
      return true;
    }

    return false;
  }

  quantidadeLetrasPalavra(): any[] {
    return Array(this.animalNome.length);
  }

  setarArrayLetras() {
    this.arrayResposta = [];
    for (this.contadorArray = 0; this.contadorArray < this.animalNome.length; this.contadorArray++) {
      this.arrayResposta[this.contadorArray] = '__';
    }
  }

  montaArrayPalavra() {
    this.arrayPalavra = [];
    for (this.contadorArray = 0; this.contadorArray < this.animalNome.length; this.contadorArray++) {
      this.arrayPalavra[this.contadorArray] = this.animalNome.substr(this.contadorArray, 1);
    }
  }

  alteraPosicao(posicao) {
    if (this.arrayResposta[posicao] === '__') {
      this.posicaoPalavra = posicao;
    } else {
      alert('Letra já preenchida!');
    }
  }

  posicaoPalavraAtiva(posicao) {
    if (this.posicaoPalavra === posicao && this.arrayResposta[posicao] === '__') {
      return 'ativo';
    }
  }

  atualizaTexto(texto) {
    if (this.jogadorAtual === 1) {
      this.textoJogo = this.nomeJogador1 + texto;
    } else if (this.jogadorAtual === 2) {
      this.textoJogo = this.nomeJogador2 + texto;
    }
  }

  resetRodada() {
    this.fimRodada = false;
    this.pontuacaoRodada = 1000;
    this.imagemSelecionada = '';
    this.indexImagem = 0;
    this.posicaoPalavra = 0;
    this.resetImagem();
    this.montaArrayPalavra();
    this.setarArrayLetras();
  }

  finalizaRodada(passaRodada = false) {
    if (passaRodada) {
      this.pontuacaoRodada = 0;
      this.atualizaPontuacao(passaRodada);
    }

    this.resetRodada();
  }

  atualizaPontuacao(passou = false) {
    if (this.jogadorAtual === 1) {
      this.pontuacaoJogador1 = this.pontuacaoJogador1 + this.pontuacaoRodada;
      if (! passou) {
        this.modalAlertStatus = 'in';
        this.modalAlertTexto1 = 'Parabéns ' + this.nomeJogadorAtual() + ', a palavra era: ' + this.animalNome + '.';
        this.modalAlertTexto2 = 'Você ganhou ' + this.pontuacaoRodada + ' pontos nessa rodada';
      }
      this.jogadorAtual = 2;
      this.atualizaTexto(' vai começar a sua rodada, se prepare...');
    } else if (this.jogadorAtual === 2) {
      this.pontuacaoJogador2 = this.pontuacaoJogador2 + this.pontuacaoRodada;
      if (! passou) {
        this.modalAlertStatus = 'in';
        this.modalAlertTexto1 = 'Parabéns ' + this.nomeJogadorAtual() + ', a palavra era: ' + this.animalNome + '.';
        this.modalAlertTexto2 = 'Você ganhou ' + this.pontuacaoRodada + ' pontos nessa rodada';
      }
      this.jogadorAtual = 1;
      this.atualizaTexto(' vai começar a sua rodada, se prepare...');
    }
  }

  nomeJogadorAtual() {
    if (this.jogadorAtual === 1) {
      return this.nomeJogador1;
    } else if (this.jogadorAtual === 2) {
      return this.nomeJogador2;
    }
  }

  resetImagem() {
    this.indexAnimal = Math.floor(Math.random() * ((this.animais.length - 1) - 0 + 1) + 0);
    this.animalNome = this.animais[this.indexAnimal]['nome'];
    this.src = this.animais[this.indexAnimal][this.indexImagem];
  }

  fechaModal() {
    this.modalAlertStatus = 'off';
    this.finalizaRodada();
  }

  iniciaJogo(jogador1, jogador2) {
    if (jogador1 !== '' && jogador2 !== '') {
      this.nomeJogador1 = jogador1;
      this.nomeJogador2 = jogador2;
      this.pontuacaoJogador1 = 0;
      this.pontuacaoJogador2 = 0;
      this.rodadasJogador1 = 0;
      this.rodadasJogador2 = 0;
      this.modalInitialStatus = 'off';
      this.atualizaTexto(' está jogando...');
      return;
    }

    alert('É necessário o preenchimento do nome dos jogadores!');
  }

  resetaJogo() {
    location.reload();
  }
}
